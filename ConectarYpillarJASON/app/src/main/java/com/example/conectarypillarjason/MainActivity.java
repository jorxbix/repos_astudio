package com.example.conectarypillarjason;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    public static Button es_buto;
    public static TextView es_texte;
    public static EditText es_texteEnviar;
    public static TextView es_texte_descifrat;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        es_buto = (Button) findViewById(R.id.btnHit);
        es_texte = (TextView) findViewById(R.id.TVJsonItem);
        es_texte_descifrat=(TextView)findViewById(R.id.lbl_textedescifrat);
        es_texteEnviar=(EditText)findViewById(R.id.txt_enviar);
        es_texteEnviar.setText("https://panel.cloudhotelier.com/apiv2");

        String putaYeisonstring="{\n" +
                "    \"resource\": \"arrivals\",\n" +
                "    \"task\": \"get\",\n" +
                "    \"access_token\": \"dkyQQ5NFPX29UQbZ\",\n" +
                "    \"access_token_secret\": \"bfvutRAxSN5pGBbVc6uY7Mw337eBHqtp\",\n" +
                "    \"data\": {\n" +
                "        \"filters\": {\n" +
                "            \"hotel_id\": \"100001\",\n" +
                "            \"start_date\": \"2020-04-01\",\n" +
                "            \"end_date\": \"2020-04-30\"\n" +
                "        }\n" +
                "    }\n" +
                "}";


        JSONObject obj= null;
        try {
            obj = new JSONObject(putaYeisonstring);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        es_texte.setText(obj.toString());
        Log.println(Log.INFO,"ASO PASSA",obj.toString());


            es_buto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //new JsonTask().execute("https://jsonparsingdemo-cec5b.firebaseapp.com/jsonData/moviesDemoItem.txt");

                    new JsonTask().execute(es_texteEnviar.getText().toString());
                    es_buto.setEnabled(false);
                }
            });


    }






        public String descifra(String esJasonString){

            //esJasonString="{\"status\":\"error\",\"code\":\"INVALID_REQUEST\",\"message\":\"Resource not defined\",\"data\":null}";
            String JSON_STRING ="{\"REBUT\":" + esJasonString + "}";
            String status, code,message,data;


            try {
                // get JSONObject from JSON file
                JSONObject obj = new JSONObject(JSON_STRING);
                // fetch JSONObject named employee
                JSONObject REBUT = obj.getJSONObject("REBUT");
                // get employee name and salary
                status = REBUT.getString("status");
                code = REBUT.getString("code");
                message = REBUT.getString("message");
                data = REBUT.getString("data");
                // set employee name and salary in TextView's


            } catch (JSONException e) {
                e.printStackTrace();
                //return "Algo ha fallat mabre";
                return JSON_STRING;
            }


        return "status: " + status + "\n" + "code: " + code + "\n"  + "message: " + message + "\n" +
                "data: " + data + "\n";

        }

        public void ejecutaDescifrar (View view){

        es_texte_descifrat.setText(descifra(es_texte.getText().toString()).toString());

        }
    public void ejecutaProva (View view) throws JSONException {

        String putaYeisonstring="{\n" +
                "    \"resource\": \"arrivals\",\n" +
                "    \"task\": \"get\",\n" +
                "    \"access_token\": \"dkyQQ5NFPX29UQbZ\",\n" +
                "    \"access_token_secret\": \"bfvutRAxSN5pGBbVc6uY7Mw337eBHqtp\",\n" +
                "    \"data\": {\n" +
                "        \"filters\": {\n" +
                "            \"hotel_id\": \"100001\",\n" +
                "            \"start_date\": \"2020-04-01\",\n" +
                "            \"end_date\": \"2020-04-30\"\n" +
                "        }\n" +
                "    }\n" +
                "}";

        JSONObject es_jsonObject=new JSONObject(putaYeisonstring);


        TextView lbl_prova=findViewById(R.id.lbl_provaYeison);
        new JsonTask2().execute(es_texteEnviar.getText().toString());



    }



}

class JsonTask extends AsyncTask<String,String,String> {

    @Override
    protected String doInBackground(String... urls) {



        BufferedReader es_lector = null;

        HttpURLConnection sa_cunexioneta = null;


        try {
            URL sa_direccioneta = new URL(urls[0]);
            sa_cunexioneta=(HttpURLConnection) sa_direccioneta.openConnection();

            sa_cunexioneta.connect();
            InputStream datusEntrants= sa_cunexioneta.getInputStream();

            es_lector=new BufferedReader(new InputStreamReader(datusEntrants));

            StringBuffer es_buffer=new StringBuffer();
            String sa_linia="";

            while((sa_linia=es_lector.readLine())!=null){
                es_buffer.append(sa_linia);
            }


            return es_buffer.toString();

        } catch (MalformedURLException e) {

            //MainActivity.showToast("DIRECCION DES SERVIDOR MAL FOTUDA");
            e.printStackTrace();
        } catch (IOException e) {
            //MainActivity.showToast("CUNEXIONETA HA FALLAT");
            e.printStackTrace();
        }finally { if(sa_cunexioneta!=null){ //si sa sa cunexioneta NO es null la desconect
            sa_cunexioneta.disconnect();}

            try { if (es_lector!=null){ //si es lector NO es null el tanc
                es_lector.close();}

            } catch (IOException e) {
                //MainActivity.showToast("CUNEXIONETA FALLADA QUAN TANCAVA");
                e.printStackTrace();
            }

        }

        return null;
    }





    @Override
    protected void onPostExecute(String resultat) {
        if (resultat==null){
            MainActivity.es_texte.setText("resultat es NULL");
            //MainActivity.showToast("No he rebut res");
            MainActivity.es_buto.setEnabled(true);
        }else{
            MainActivity.es_texte.setText(resultat);
            //MainActivity.showToast("He rebut coseta");
            super.onPostExecute(resultat);}
            MainActivity.es_buto.setEnabled(true);
    }


    private String getJSON(String URL, JSONObject obj) throws JSONException {
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(URL);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/json");
            connection.setUseCaches (false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(30000);
            //Send request
            DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream ());
            wr.write(obj.toString().getBytes("UTF-8"));
            wr.flush ();
            wr.close ();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {

            e.printStackTrace();
            return e.toString();

        } finally {

            if(connection != null) {
                connection.disconnect();
            }
        }

    }


}
class JsonTask2 extends AsyncTask<String,String,String> {

    @Override
    protected String doInBackground(String... urls) {

        HttpURLConnection connection = null;
        String putaYeisonstring="{\n" +
                "    \"resource\": \"arrivals\",\n" +
                "    \"task\": \"get\",\n" +
                "    \"access_token\": \"dkyQQ5NFPX29UQbZ\",\n" +
                "    \"access_token_secret\": \"bfvutRAxSN5pGBbVc6uY7Mw337eBHqtp\",\n" +
                "    \"data\": {\n" +
                "        \"filters\": {\n" +
                "            \"hotel_id\": \"100001\",\n" +
                "            \"start_date\": \"2020-04-01\",\n" +
                "            \"end_date\": \"2020-04-30\"\n" +
                "        }\n" +
                "    }\n" +
                "}";


        JSONObject obj= null;
        try {
            obj = new JSONObject(putaYeisonstring);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            //Create connection
            URL url = new URL("https://panel.cloudhotelier.com/apiv2");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/json");
            connection.setUseCaches (false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(30000);
            //Send request
            DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream ());
            wr.write(obj.toString().getBytes("UTF-8"));
            wr.flush ();
            wr.close ();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {

            e.printStackTrace();
            return e.toString();

        } finally {

            if(connection != null) {
                connection.disconnect();
            }
            return null;}

    }





    @Override
    protected void onPostExecute(String resultat) {
        if (resultat==null){
            MainActivity.es_texte.setText("resultat es NULL");
            //MainActivity.showToast("No he rebut res");
            MainActivity.es_buto.setEnabled(true);
        }else{
            MainActivity.es_texte.setText(resultat);
            //MainActivity.showToast("He rebut coseta");
            super.onPostExecute(resultat);}
        MainActivity.es_buto.setEnabled(true);
    }




}


